# MultiBlindTest

## Pré-requis

### Ajouter les variables d'environnements du dossier path
Copier le chemin vers le dossier *path* du projet.  
Rechercher __Modifier les variables d'environnement système__ dans la barre de recherche windows.  
Cliquer sur __Variables d'environnment__.  
Chercher la variable nommée *path* dans la partie __Variables système__.  
Cliquer sur __Modifier__ puis sur __Nouveau__.  
Coller le chemin du dossier *path* du projet.  

/!\ Si vous changez de place le dossier du projet il faut changer le chemin dans les variables d'environnement.  

  
### Placer les musiques dans le dossier 'Musiques' au format mp3.  
Par défaut le programme regarde s'il existe un dosser __Musiques__ en son sein.  
S'il n'y a pas de dossier __Musiques__ une fenêtre s'ouvre vous demandant de choisir un dossier dans lequel vos musiques sont stockées.  
Si vous n'avez pas sépcialement de dossier de musiques, il est recommandé de créer un dossier __Musiques__ à l'endroit où se trouvent les fichiers *main.py* et *main.exe* et d'y stocker les musiques au format mp3 afin de gagner du temps au démarrage.  

### Installer la police
Pour bénéficier d'une police plus jolie vous pouvez installer la police présente dans le dossier __fonts__.  

## Lancement

### Python
python -m venv venv  
.\venv\Scripts\Acivate.ps1  
pip install -r .\requirements.txt  
python main.py  

### .Exe
Lancer l'exécutable main.exe  

## Fonctionnement
Sélectionner les différentes musiques à combiner et cliquer sur le bouton "mixer".  
Lancer ensuite le mix avec le bouton play.  
A chaque son trouvé, cliquer sur stop et dé-séléctionner le son trouvé.  
Ré-appuyer sur play.  
Continuer jusqu'à ce que toutes les musiques soient trouvées.  
Les deux dernières musiques doivent être trouvées ensembles.  

