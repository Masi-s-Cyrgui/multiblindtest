# -*- coding: utf-8 -*-
import os
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "YES"
import tkinter as tk
from tkinter import Listbox, Scrollbar, Button, simpledialog, ttk, filedialog
from PIL import Image, ImageTk
import threading
from analyse import mix_multiple_mp3, play_multiple_mp3, stop_songs
from download_mp3 import download, download_playlist


class AudioPlayerApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Multi Blind Test")
        icon_path = "Icones/icone.ico"
        self.master.iconbitmap(icon_path)
        self.master.wm_state('zoomed')
        self.the_path = "Musiques/"

        self.mix_icon = ImageTk.PhotoImage(Image.open("Icones/mixer.png").resize((75, 75), Image.Resampling.LANCZOS))
        self.play_icon = ImageTk.PhotoImage(Image.open("Icones/play.png").resize((75, 75), Image.Resampling.LANCZOS))
        self.stop_icon = ImageTk.PhotoImage(Image.open("Icones/stop.png").resize((75, 75), Image.Resampling.LANCZOS))
        self.add_icon = ImageTk.PhotoImage(Image.open("Icones/add.png").resize((75, 75), Image.Resampling.LANCZOS))
        self.folder_icon = ImageTk.PhotoImage(Image.open("Icones/folder.png").resize((75, 75), Image.Resampling.LANCZOS))
 
        font_spec = ("Milk Mango", 12)

        if not os.path.isdir(self.the_path):
            os.mkdir("Musiques")
      
        self.paths = [self.the_path + path for path in os.listdir(self.the_path) if os.path.isfile(os.path.join(self.the_path, path))]
        self.selected_indices = []

        container = tk.Frame(master)
        container.pack(expand=True, fill=tk.BOTH)

        self.listbox = Listbox(container, selectmode=tk.MULTIPLE)
        self.listbox.pack(pady=10, side=tk.LEFT, expand=True, fill=tk.BOTH)

        for path in self.paths:
            self.listbox.insert(tk.END, os.path.basename(path))
       
        scrollbar = Scrollbar(container, command=self.listbox.yview)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.listbox.config(yscrollcommand=scrollbar.set)

        button_frame = tk.Frame(master)
        button_frame.pack(anchor='c', padx=(0, 10))

        self.mix_button = Button(button_frame, text="Mix", image=self.mix_icon, command=self.mix_selected, compound=tk.TOP, bd=0, font=font_spec)
        self.mix_button.pack(padx=5, side=tk.LEFT)

        self.play_button = Button(button_frame, text="Play", image=self.play_icon, command=self.play_selected, compound=tk.TOP, bd=0, font=font_spec)
        self.play_button.pack(padx=5, side=tk.LEFT)

        self.stop_button = Button(button_frame, text="Stop", image=self.stop_icon, command=stop_songs, compound=tk.TOP, bd=0, font=font_spec)
        self.stop_button.pack(padx=5, side=tk.LEFT)

        self.add_button = Button(button_frame, text="Add", image=self.add_icon, command=self.add_song, compound=tk.TOP, bd=0, font=font_spec)
        self.add_button.pack(padx=5, side=tk.LEFT)

        self.add_playlist_button = Button(button_frame, text="Playlist", image=self.add_icon, command=self.add_playlist, compound=tk.TOP, bd=0, font=font_spec)
        self.add_playlist_button.pack(padx=5, side=tk.LEFT)

        self.change_path_button = Button(button_frame, text="Folder", image=self.folder_icon, command=self.change_path, compound=tk.TOP, bd=0, font=font_spec)
        self.change_path_button.pack(padx=5, side=tk.LEFT)

        self.master.resizable(True, True)

        self.master.bind("<Control-f>", self.on_ctrl_f)

    def on_ctrl_f(self, event):
        search_query = simpledialog.askstring("Search", "Entrer une musique:")
        if search_query is not None:
            self.search_listbox(search_query)

    def search_listbox(self, query):
        self.listbox.selection_clear(0, tk.END)
        first_match_index = None

        for i, path in enumerate(self.paths):
            if query.lower() in path.lower():
                self.listbox.selection_set(i)

                if first_match_index is None:
                    first_match_index = i

        if first_match_index is not None:
            self.listbox.see(first_match_index)

    def play_selected(self):
        global dic_mp3
        self.selected_indices = self.listbox.curselection()
        if self.selected_indices:
            play_multiple_mp3(self.selected_indices, dic_mp3)


    def mix_selected(self):
        global dic_mp3
        self.selected_indices = self.listbox.curselection()
        if self.selected_indices:
            selected_paths = [self.paths[i] for i in self.selected_indices]
            list_mp3 = mix_multiple_mp3(selected_paths)
            dic_mp3 = dict(zip(self.selected_indices, list_mp3))


    # def stop_songs(self):
    #     pygame.mixer.stop()

    def add_song(self):
        url = simpledialog.askstring("Search", "Entrer l'url d'une musique:")
        self.download_and_refresh(url, "song")

    def add_playlist(self):
        url = simpledialog.askstring("Search", "Entrer l'url d'une playlist:")
        self.download_and_refresh(url, "playlist")

    def change_path(self):
        folder_path = filedialog.askdirectory()
        if folder_path:
            self.the_path = folder_path + "/"  
            self.paths = [self.the_path + path for path in os.listdir(self.the_path) if os.path.isfile(os.path.join(self.the_path, path))]
            self.listbox.delete(0, tk.END)
            for path in self.paths:
                self.listbox.insert(tk.END, os.path.basename(path))
            

    
    def download_and_refresh(self, url, mode):
        # Create a progress bar dialog
        progress_dialog = ProgressDialog(self.master, "Downloading...", 100)

        def download_task():
            nonlocal progress_dialog
            try:
                if mode == "song":
                    download(url, self.the_path, progress_callback=progress_dialog.update_progress)
                else:
                    download_playlist(url, self.the_path, progress_callback=progress_dialog.update_progress)

                self.paths = [self.the_path + path for path in os.listdir(self.the_path) if os.path.isfile(os.path.join(self.the_path, path))]
                self.listbox.delete(0, tk.END)
                for path in self.paths:
                    self.listbox.insert(tk.END, os.path.basename(path))
            finally:
                # Close the progress bar dialog after the task is complete
                progress_dialog.close()

        # Create a thread for the download task
        download_thread = threading.Thread(target=download_task)
        download_thread.start()

        # Show the progress bar dialog
        progress_dialog.show()



class ProgressDialog:
    def __init__(self, master, title, max_value):
        self.top = tk.Toplevel(master)
        self.top.title(title)
        self.top.geometry("300x50")
        self.progressbar = ttk.Progressbar(self.top, length=250, mode="determinate", maximum=max_value)
        self.progressbar.grid(row=0, column=0, padx=20, pady=10)
        self.top.withdraw()

    def show(self):
        self.progressbar.start()
        self.top.deiconify()

    def update_progress(self, value):
        self.progressbar.stop()
        self.progressbar["value"] = value

    def close(self):
        self.progressbar.stop()
        self.top.destroy()


if __name__ == "__main__":
    dic_mp3 = {}
    root = tk.Tk()
    app = AudioPlayerApp(root)
    root.mainloop()