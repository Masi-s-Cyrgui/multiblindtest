from pytube import YouTube, Playlist
import os
from pydub import AudioSegment
import threading

def download(link, path, progress_callback=None):
    yt = YouTube(link)
    mp3 = yt.streams.filter(only_audio=True).first()
    if progress_callback:
        progress_callback(25)
    out_file = mp3.download(path)
    if progress_callback:
        progress_callback(50)
    base, ext = os.path.splitext(out_file)
    mp3_file = f"{base}.mp3"
    audio = AudioSegment.from_file(out_file, format="mp4")
    audio.export(mp3_file, format="mp3")
    if progress_callback:
        progress_callback(75)
    os.remove(out_file)
    if progress_callback:
        progress_callback(100)
    
def download_playlist(link, path, progress_callback=None):
    playlist = Playlist(link)
    
    def download_and_update_progress(url):
        download(url, path)
        if progress_callback:
            progress_callback(100 // len(playlist))

    threads = []
    for url in playlist:
        thread = threading.Thread(target=download_and_update_progress, args=(url,))
        threads.append(thread)
        thread.start()

    # Wait for all threads to finish
    for thread in threads:
        thread.join()

    if progress_callback:
        progress_callback(100)

if __name__ == "__main__":
    download_playlist("https://www.youtube.com/playlist?list=PLY37FwYZkYU4IJdoqnh69jVEF2GpqDxvh")