from pydub import AudioSegment
import numpy as np
import pygame

def get_amplitude_envelope(audio_segment, interval_ms=1000):
    samples_per_interval = int(audio_segment.frame_rate * (interval_ms / 1000))
    samples = np.array(audio_segment.get_array_of_samples())
    envelope = []

    for i in range(0, len(samples), samples_per_interval):
        interval_samples = samples[i:i+samples_per_interval]
        envelope.append(np.max(np.abs(interval_samples)))

    return np.array(envelope)

def pad_or_truncate(array, length):
    if len(array) < length:
        return np.pad(array, (0, length - len(array)))
    elif len(array) > length:
        return array[:length]
    else:
        return array

def find_common_max_amplitude_time(audio_segments, interval_ms=1000):
    

    for audio_segment in audio_segments:
        song_duration = audio_segment.duration_seconds
        audio_segment.frame_rate = len(audio_segment.get_array_of_samples()) / song_duration
    # Get amplitude envelopes for all audio segments
    amplitude_envelopes = [get_amplitude_envelope(seg, interval_ms) for seg in audio_segments]

    # Find the maximum length among amplitude envelopes
    max_length = max(len(envelope) for envelope in amplitude_envelopes)

    # Pad or truncate amplitude envelopes to the maximum length
    amplitude_envelopes = [pad_or_truncate(envelope, max_length) for envelope in amplitude_envelopes]

    # Compute the average amplitude at each time point
    average_amplitude = np.mean(amplitude_envelopes, axis=0)

    # Find the time point with the maximum average amplitude
    max_amplitude_index = np.argmax(average_amplitude)
    max_amplitude_time = max_amplitude_index * interval_ms

    return max_amplitude_time




def adjust_loudness(audio, target_loudness):

    # Calculate the adjustment factor
    adjustment_factor = target_loudness - audio.dBFS

    # Adjust the loudness of the audio file
    adjusted_audio = audio + adjustment_factor

    return adjusted_audio


def mix_multiple_mp3(file_paths):
    # Load audio segments for all songs
    audio_segments = [AudioSegment.from_file(path) for path in file_paths]

    # Find the common moment of highest amplitude
    common_max_amplitude_time = find_common_max_amplitude_time(audio_segments)

    # Create segments starting at the specified start time within the song
    segments = [sound[(common_max_amplitude_time/2):] for sound in audio_segments]
    
    # Find the minimal DB in news segments
    min_loudness = min((segment.dBFS for segment in segments if segment.dBFS > float('-inf')))

    # Put all the segments at the minimum DB
    adjusted_segments = [adjust_loudness(segment, min_loudness) for segment in segments]

    return adjusted_segments


def play_multiple_mp3(indices, segments):
    pygame.mixer.init()


    # Max number of songs played simultaneously
    pygame.mixer.set_num_channels(16)

    # Start mixer channels for all MP3 files
    channels = [pygame.mixer.Channel(i) for i in range(len(indices))]

    # Creating a new list with only the songs selectionned but with the segments already mixed
    new_segments = [segments[key] for key in indices if key in segments]

    # Play each sound after the calculated delay
    for channel, segment in zip(channels, new_segments):
        # Convert the Pydub AudioSegment to a Pygame Sound
        pygame_sound = pygame.mixer.Sound(buffer=segment.raw_data)
        channel.play(pygame_sound)

def stop_songs():
    pygame.mixer.stop()