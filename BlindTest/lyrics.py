import tkinter as tk
from tkinter import Button, Entry, Label, messagebox
from tkinter import Scrollbar, Text
from PIL import Image, ImageTk
import lyricsgenius
from langdetect import detect
from googletrans import Translator

class LyricsApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Lyrics Search")
        icon_path = "Icones/icone.ico"
        self.master.iconbitmap(icon_path)
        self.master.wm_state('zoomed')

        self.go_icon = ImageTk.PhotoImage(Image.open("Icones/go.png").resize((50, 50), Image.Resampling.LANCZOS))

        font_spec = ("Milk Mango", 12)

        self.artist_label = Label(master, text="Artist:", font=font_spec)
        self.artist_label.pack(pady=5)

        self.artist_entry = Entry(master, font=font_spec)
        self.artist_entry.pack(pady=5)

        self.song_label = Label(master, text="Song:", font=font_spec)
        self.song_label.pack(pady=5)

        self.song_entry = Entry(master, font=font_spec)
        self.song_entry.pack(pady=5)

        self.go_button = Button(master, text="Go", image=self.go_icon, command=self.search_lyrics, font=font_spec)
        self.go_button.pack(pady=10)

        self.lyrics_text = Text(master, wrap=tk.WORD)
        self.lyrics_text.pack(side = tk.LEFT, expand=True, fill=tk.BOTH, padx=10, pady=10)

        self.result_text = Text(master, wrap=tk.WORD, height=10)
        self.result_text.pack(expand=True, fill=tk.BOTH, padx=10, pady=10, side=tk.RIGHT)


    def search_lyrics(self):
        artist = self.artist_entry.get().strip()
        song = self.song_entry.get().strip()

        if not artist or not song:
            messagebox.showerror("Error", "Please enter both artist and song.")
            return

        
        genius = lyricsgenius.Genius("rWeALr0l2YakZFJg4U2yzh5XH5KMupaSmuTvU1us4jMsAlogPBM5d8FqNXnHWXO-")
        try:
            song = genius.search_song(song, artist)
            if song is not None:
                lyrics = song.lyrics
                lyrics_list = lyrics.split('\n')
                lyrics_list = [line for line in lyrics_list if '[' not in line]
                lyrics = '\n'.join(lyrics_list)
                self.lyrics_text.delete(1.0, tk.END)
                self.lyrics_text.insert(tk.END, lyrics)
                detected_language = detect(lyrics)
                translator = Translator()
                if detected_language == 'fr':
                    translated_text = translator.translate(lyrics, dest='en').text
                else:
                    translated_text = translator.translate(lyrics, dest='fr').text
                self.result_text.delete(1.0, tk.END)
                self.result_text.insert(tk.END, translated_text)

            else:
                messagebox.showerror("Error", "Lyrics not found for the given artist and song.")
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {str(e)}")

if __name__ == "__main__":
    root = tk.Tk()
    lyrics_app = LyricsApp(root)
    root.mainloop()