import tkinter as tk
from tkinter import Button, Frame
from PIL import Image, ImageTk
from multiblindtest import AudioPlayerApp
from lyrics import LyricsApp

class MainMenuApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Main Menu")
        icon_path = "Icones/icone.ico"
        self.master.iconbitmap(icon_path)

        self.blind_test_icon = ImageTk.PhotoImage(Image.open("Icones/blindtest.png").resize((100, 100), Image.Resampling.LANCZOS))
        self.lyrics_icon = ImageTk.PhotoImage(Image.open("Icones/lyrics.png").resize((100, 100), Image.Resampling.LANCZOS))

        font_spec = ("Milk Mango", 12)

        menu_frame = Frame(master)
        menu_frame.pack(expand=True, fill=tk.BOTH)

        blind_test_button = Button(menu_frame, text="Multi Blind Test", image=self.blind_test_icon, command=self.open_audio_player, compound=tk.TOP, bd=0, font=font_spec)
        blind_test_button.pack(padx=10, pady=10, side=tk.LEFT)

        lyrics_button = Button(menu_frame, text="Lyrics", image=self.lyrics_icon, command=self.open_lyrics, compound=tk.TOP, bd=0, font=font_spec)
        lyrics_button.pack(padx=10, pady=10, side=tk.LEFT)

        self.master.resizable(True, True)

    def open_audio_player(self):
        self.master.withdraw()
        audio_player_window = tk.Toplevel(self.master)
        audio_player_app = AudioPlayerApp(audio_player_window)
        audio_player_window.protocol("WM_DELETE_WINDOW", lambda: self.on_close_audio_player(audio_player_window))

    def on_close_audio_player(self, window):
        window.destroy()
        self.master.deiconify()

    def open_lyrics(self):
        self.master.withdraw()
        lyrics_window = tk.Toplevel(self.master)
        lyrics_app = LyricsApp(lyrics_window)
        lyrics_window.protocol("WM_DELETE_WINDOW", lambda: self.on_close_audio_player(lyrics_window))

if __name__ == "__main__":
    root = tk.Tk()
    main_menu_app = MainMenuApp(root)
    root.mainloop()